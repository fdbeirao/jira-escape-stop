const graceSeconds = 5;
var lastEditStartedAt = undefined;

if (currentPageIsJIRA()) {
  window.addEventListener("keyup", checkPreventEscape, true);
  document.getElementById("description-val").onclick = function() { lastEditStartedAt = new Date(); };
  log("Current page has been determined to be JIRA");
}

function checkPreventEscape() {
  if (event.keyCode == 27 && document.activeElement.id == "description") {
    if (lastEditStartedAt === undefined || ((new Date() - lastEditStartedAt) / 1000) >= graceSeconds) {
      log(`🛑 Escape stopped!`);
      event.stopPropagation();
    } else {
      log(`✅ Escape allowed (less than ${graceSeconds} seconds elapsed since starting to edit)`)
    }
  }
}

function currentPageIsJIRA() {
  return document.head.querySelector("meta[name='application-name'][content='JIRA'][data-name='jira']") !== null;
}

function log(input) {
  console.log(`🚫 Jira escape stop: ${input}`);
}
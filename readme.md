# Jira escape stop

This extension exists (==will exist==) with the purpose of preventing my boss (and hopefully more people) from trying to go full Spartan on a computer, because they spent 20+ minutes editing a ticket/user story/whatever in [Jira](https://www.atlassian.com/software/jira), and either accidentally or out of habit from other text editors, pressed `esc` and _puff_.. all your beloved text, all the edit you were making.. _puff_.. gone.

## TODOs:

- [x] [Learn how to write a chrome extension](https://developer.chrome.com/extensions/getstarted)
- [x] Figure out which fields in JIRA have this behaviour
- [x] Figure out how to capture the `esc` keypress and intercept it
- [x] ❓❓❓
- [ ] 💰💰💰
- [x] Deploy to [chrome web store](https://chrome.google.com/webstore/detail/jira-escape-stop/cddliagekbhfoancaghfnmolghpknlbe)